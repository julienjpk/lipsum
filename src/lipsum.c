#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_STDIO_H
#include <stdio.h>
#endif

#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef HAVE_CURL_CURL_H
#include <curl/curl.h>
#endif

#ifndef UNITS_MIN
/* Minimal number of text units. */
#define UNITS_MIN 1
#endif

#ifndef UNITS_MAX
/* Maximal number of text units. */
#define UNITS_MAX 100
#endif

#define URLSIZ 64
#define URL "http://www.lipsum.com/feed/xml?amount=%d&start=%s&what=%s"

/* Usage information. */
void usage();
/* cURL write callback. */
size_t curl_print(char* buf, size_t size, size_t nmemb, void* userdata);

int main(int argc, char* argv[])
{
    char *structures[] = { "paras", "words", "bytes", "lists" };
    char c, request_str[URLSIZ];
    CURLcode curl_res;
    CURL *curl;

    int n_sections = UNITS_MIN;
    int struct_idx = 0;
    int lipsum_start = 0;
    int immediate_end = 0;

    opterr = 0;
    while((c = getopt(argc, argv, "n:pwblsivh")) > 0){
	switch(c){
	    case 'n':
		n_sections = (int)strtol(optarg, NULL, 10);
		if(n_sections < UNITS_MIN || n_sections > UNITS_MAX){
		    fprintf(stderr, "Too few/many sections requested with -n.\n");
		    return EXIT_FAILURE;
		}
		break;

	    case 'h':
		usage();
		return EXIT_SUCCESS;
		break;

	    case 'v':
		printf("%s\n", PACKAGE_STRING);
		return EXIT_SUCCESS;
		break;

	    /* struct_idx contains an index for the structures array. */
	    case 'p': struct_idx = 0; break;
	    case 'w': struct_idx = 1; break;
	    case 'b': struct_idx = 2; break;
	    case 'l': struct_idx = 3; break;
	    case 's': lipsum_start = 1; break;
	    case 'i': immediate_end = 1; break;

	    default:
		usage();
		return EXIT_FAILURE;
	}
    }

    /* Thanks to lipsum.com's Python tool for the format, wouldn't have found it
     * otherwise. */
    sprintf(request_str, URL, n_sections,
	    (lipsum_start ? "yes" : "no"),
	    structures[struct_idx]);

    curl = curl_easy_init();
    if(!curl){
	fprintf(stderr, "cURL could not be initialised.\n");
	return EXIT_FAILURE;
    }

    /* Sending the data to curl_print. */
    curl_easy_setopt(curl, CURLOPT_URL, request_str);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curl_print);
    curl_res = curl_easy_perform(curl);

    if(curl_res != CURLE_OK){
	fprintf(stderr, "cURL error: %s.\n", curl_easy_strerror(curl_res));
	curl_easy_cleanup(curl);
	return EXIT_FAILURE;
    }

    if(!immediate_end) printf("\n");
    
    curl_easy_cleanup(curl);
    return EXIT_SUCCESS;
}

void usage(){
    fprintf(stderr,
	    "Usage: %s [ -pwblsi ] [ -n N ]\n"
	    "See lipsum(7) for more information.\n\n"
	    "    -p, -w, -b and -l are used to set the output style.\n"
	    "    -s makes the text start with \"Lorem ipsum...\".\n"
	    "    -i removes the final newline (end immediately).\n"
	    "    -n (%d <= N <= %d) sets the number of units to print.\n",
	    PACKAGE_NAME, UNITS_MIN, UNITS_MAX);
}

size_t curl_print(char* buf, size_t size, size_t nmemb, void* userdata){
    static int started = 0; /* 1 if <lipsum> has been encountered. */
    char *mark = buf, *stop = NULL;

    if(!started){
	/* Assuming buf isn't inconveniently truncated... */
	if(mark = strstr(buf, "<lipsum>")){
	    mark += 8;
	    started = 1;
	}
	else return size * nmemb;
    }

    /* Mark the end of the string at </lipsum>. */
    if((stop = strstr(mark + 8, "<"))) *stop = 0;
    printf("%s", mark);

    return size * nmemb;
}
